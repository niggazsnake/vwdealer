# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modelcluster.fields import ParentalKey

from django.db import models
from wagtail.wagtailcore.models import Page, Orderable

from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
    ]

    class Meta:
        abstract = True


class TeazerPage(LinkFields):
    class Meta:
        verbose_name = u'Тизер'
        abstract = True

    image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Фото')

    panels = [
        ImageChooserPanel('image'),
        MultiFieldPanel(LinkFields.panels, "Ссылка на источник"),
    ]


class TeazerIndexPageItems(Orderable, TeazerPage):
    page = ParentalKey('TeazerIndexPage', related_name='teazer_items')


class TeazerIndexPage(Page):
    class Meta:
        verbose_name = u'Список тизеров'


TeazerIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    InlinePanel('teazer_items', label="Тизер"),
]
