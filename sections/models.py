# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modelcluster.fields import ParentalKey

from django.db import models

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField

from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from cars.models import CarPage


class SectionIndexPage(Page):
    class Meta:
        verbose_name = u'Список разделов сайта'

    subpage_types = ['MainSectionPage', 'cars.CarIndexPage']


class ArticlePage(Page):
    class Meta:
        verbose_name = u'Страница раздела'

    image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Картинка на странице', related_name='images')
    name = models.CharField(u'Заголовок страницы', max_length=250)
    content = RichTextField(u'Текст страницы')
    desc = RichTextField(u'Краткое описание')
    desc_image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Картинка для краткого описания',
                                   related_name='desc_images')
    date = models.DateTimeField(u'Дата добавления', auto_now=True)
    car = ParentalKey(CarPage, related_name='cars', blank=True, null=True)

    content_panels = Page.content_panels + [
        ImageChooserPanel('image'),
        ImageChooserPanel('desc_image'),
        FieldPanel('name'),
        FieldPanel('desc'),
        FieldPanel('content'),
        PageChooserPanel('car'),
    ]


class MainSectionPage(Page):
    class Meta:
        verbose_name = u'Главный раздел сайт'

    name = models.CharField(u'Название раздела', max_length=250)
    image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Картинка в разделе', blank=True, null=True)

    content_panels = Page.content_panels + [
        FieldPanel('name'),
        ImageChooserPanel('image'),
    ]

    subpage_types = ['SectionPage', 'ArticlePage']

    @property
    def sections(self):
        return SectionPage.objects.live().descendant_of(self)

    @property
    def articles(self):
        articles = ArticlePage.objects.live().descendant_of(self).filter(depth__lt=6)
        articles = articles.order_by('-date')
        return articles


class SectionPage(Page):
    class Meta:
        verbose_name = u'Раздел сайта'

    name = models.CharField(u'Название раздела', max_length=250)
    image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Картинка в разделе', blank=True, null=True)
    desc = RichTextField(u'Краткое описание')
    desc_image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Картинка для краткого описания',
                                   related_name='section_images', blank=True, null=True)

    content_panels = Page.content_panels + [
        FieldPanel('name'),
        ImageChooserPanel('image'),
        FieldPanel('desc'),
        ImageChooserPanel('desc_image'),
    ]

    subpage_types = ['ArticlePage']


    @property
    def articles(self):
        articles = ArticlePage.objects.live().descendant_of(self)
        articles = articles.order_by('-date')
        return articles


# SectionPage.content_panels = [
# InlinePanel('preview_items'),
#]


#class SectionPreviewLinks(Orderable, SectionPreviewPage):
#    page = ParentalKey('SectionPage', related_name='preview_items')





