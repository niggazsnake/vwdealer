# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from modelcluster.fields import ParentalKey

from django.db import models
from django.db.models import Min

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailadmin.edit_handlers import InlinePanel

from teazers.models import TeazerPage
from home.models import LinkFields


class CarIndexPage(Page):
    class Meta:
        verbose_name = u'Список моделей'

    subpage_types = ['cars.CarPage']

    @property
    def cars(self):
        cars = CarPage.objects.live().descendant_of(self)
        return cars


class CarPage(Page):
    class Meta:
        verbose_name = u'Модель'

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        related_name='+',
        verbose_name=u"Фото на главной"
    )
    thumbnail = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=u"Миниатюра модели"
    )

    name = models.CharField(verbose_name=u"Название модели", max_length=100)
    banner_title = RichTextField(verbose_name=u'Заголовок на банере', max_length=150)
    banner_sub_title = RichTextField(verbose_name=u'Второй заголовок на банере', max_length=150)

    content_panels = Page.content_panels + [
        FieldPanel('name', classname="full"),
        FieldPanel('banner_title', classname="full"),
        FieldPanel('banner_sub_title', classname="full"),
        ImageChooserPanel('image'),
        ImageChooserPanel('thumbnail'),
    ]

    @property
    def min_price(self):
        min_price = EquipmentPage.objects.live().descendant_of(self)
        min_price = min_price.aggregate(min=Min('price'))
        return min_price['min']

    @property
    def teazers(self):
        teazers = TeazerPage.objects.live().descendant_of(self)
        return teazers

    subpage_types = ['OverviewPage', 'GalleryIndexPage', 'FileIndexPage', 'EquipmentIndexPage']


class OverviewPage(Page):
    class Meta:
        verbose_name = u'Обзор'

    body = RichTextField(u'Обзор')

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]


class GalleryPage(LinkFields):
    class Meta:
        verbose_name = u'Фото в галерею(модели авто)'
        abstract = True

    image = models.ForeignKey('wagtailimages.Image', verbose_name=u"Фото для галереи")
    title_image = models.CharField(u"Заголовок слайда", max_length=50, blank=True)
    content = RichTextField(verbose_name="Заголовок на банере", blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('title_image', classname="full"),
        FieldPanel('content', classname="full"),
    ]


class GalleryIndexPageItems(Orderable, GalleryPage):
    page = ParentalKey('GalleryIndexPage', related_name='gallery_images')


class GalleryIndexPage(Page):
    class Meta:
        verbose_name = u'Галерея для авто'


GalleryIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    InlinePanel('gallery_images', label='Фото в галлерею'),
]


class FilePage(LinkFields):
    class Meta:
        verbose_name = u'Фвйл для авто'
        abstract = True


    file = models.ForeignKey('wagtaildocs.Document', verbose_name=u'Документ')

    panels = [
        DocumentChooserPanel('file'),
    ]


class FileIndexPageItems(Orderable, FilePage):
    page = ParentalKey('FileIndexPage', related_name='files')


class FileIndexPage(Page):
    class Meta:
        verbose_name = u'Список файлов для авто'


FileIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    InlinePanel('files', label='Добавить файл'),
]


class EquipmentIndexPage(Page):
    class Meta:
        verbose_name = u'Список комплектаций'

    subpage_types = ['cars.EquipmentPage']

    @property
    def equipments(self):
        equipments = EquipmentPage.objects.live().descendant_of(self)
        return equipments


class EquipmentPage(Page):
    class Meta:
        verbose_name = u'Комплектация'

    image = models.ForeignKey('wagtailimages.Image', verbose_name=u"Фото")
    title_eq = models.CharField(max_length=100, verbose_name=u"Заголовок")
    name = models.CharField(max_length=100, verbose_name=u"Название")
    price = models.PositiveIntegerField(verbose_name=u"Цена")
    desc = RichTextField(u"Полное описание")

    content_panels = Page.content_panels + [
        FieldPanel('title_eq'),
        ImageChooserPanel('image'),
        FieldPanel('name'),
        FieldPanel('price'),
        FieldPanel('desc'),
    ]