# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index


from allmenu.models import LinkFields, TeazerPage, TeazerIndexPage


class HomePage(Page):
    class Meta:
        verbose_name = u'Главная страница'

    @property
    def slides(self):
        slides = SlidePage.objects.live().descendant_of(self)
        return slides

    @property
    def teazers(self):
        teazers = TeazerPage.objects.live().descendant_of(self)
        return teazers

#HomePage.content_panels = [
#    InlinePanel('slides'),
#]


class SlidePage(Page, LinkFields):
    title_page = models.CharField(u"Заголовок слайда", max_length=250)
    date_page = models.DateField(auto_created=True)
    image_page = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('title'),
    )

    content_panels = Page.content_panels + [
        FieldPanel('title_page'),
        ImageChooserPanel('image_page'),
        MultiFieldPanel(LinkFields.panels, "Ссылка на источник"),
        FieldPanel('date_page'),
    ]

#class HomePageSlidePageItems(Orderable, SlidePage):
#    page = ParentalKey('HomePage', related_name='slides')

"""class SliderIndexPage(Page):
    subpage_types = ['SliderPage', 'allmenu.TeazerIndexPage']

    @property
    def slides(self):
        slides = SliderPage.objects.live().descendant_of(self)
        return slides

    @property
    def teazers(self):
        teazers = TeazerPage.objects.live().descendant_of(self)
        return teazers


"""