# -*- coding: utf-8 -*-
from django import template


register = template.Library()

@register.assignment_tag(takes_context=True)
def get_site_root(context):
    # NB this returns a core.Page, not the implementation-specific model used
    # so object-comparison to self will return false as objects would differ
    return context['request'].site.root_page


def has_menu_children(page):
    return page.get_children().live().in_menu().exists()

@register.inclusion_tag('home/tags/menu.html', takes_context=True)
def main_menu(context, parent, calling_page=None):

    menu_items = parent.get_children().live().in_menu()
    for menuitem in menu_items:
        #menuitem.show_dropdown = has_menu_children(menuitem)
        # We don't directly check if calling_page is None since the template
        # engine can pass an empty string to calling_page
        # if the variable passed as calling_page does not exist.
        menuitem.active = (calling_page.url.startswith(menuitem.url)
                           if calling_page else False)
    return {
        'calling_page': calling_page,
        'menuitems': menu_items,
        'request': context['request'],
    }