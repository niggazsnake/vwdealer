from __future__ import unicode_literals

from django.apps import AppConfig


class AllmenuConfig(AppConfig):
    name = 'allmenu'
