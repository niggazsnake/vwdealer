# -*- coding: utf-8 -*-
from django import template
from allmenu.models import *

register = template.Library()


@register.inclusion_tag('allmenu/main_menu.html', takes_context=True)
def main_menu(context):
    return {
        #'menu': MainMenuPage.objects.all()
        'menu': SectionMenu.objects.all()
    }

@register.inclusion_tag('allmenu/car_menu.html', takes_context=True)
def car_menu(context):
    return {
        #'menu': MainMenuPage.objects.all()
        'car_menu': CarMenuPage.objects.all()
    }