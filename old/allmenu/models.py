# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

from modelcluster.fields import ParentalKey

from django.db import models
from django.db.models import Min

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailadmin.edit_handlers import InlinePanel


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
    ]

    class Meta:
        abstract = True


class TeazerPage(Page, LinkFields):
    class Meta:
        verbose_name = u'Элемент в тизер'

    image = models.ForeignKey('wagtailimages.Image', verbose_name=u'Фото')

    content_panels = Page.content_panels + [
        ImageChooserPanel('image'),
        MultiFieldPanel(LinkFields.panels, "Ссылка на источник"),
    ]


class TeazerIndexPage(Page):
    class Meta:
        verbose_name = u'Тизер'

    subtype_pages = ['allmenu.TeazerPage']


class MenuItemPage(LinkFields):
    class Meta:
        verbose_name = u'Элемент в меню'
        abstract = True

    name_link = models.CharField(u"Текст ссылки", max_length=200)

    panels = [
        FieldPanel('name_link', classname="full"),
        MultiFieldPanel(LinkFields.panels, u"Ссылка"),
    ]


class MenuItemsPage(Orderable, MenuItemPage):
    page = ParentalKey('allmenu.SectionMenu', related_name='menu_items')


class SectionMenu(Page, LinkFields):
    class Meta:
        verbose_name = u'Раздел'

    name_link = models.CharField(u"Текст ссылки", max_length=200)

    content_panels = Page.content_panels + [
        FieldPanel('name_link', classname="full"),
        MultiFieldPanel(LinkFields.panels, u"Ссылка"),
    ]


SectionMenu.content_panels = SectionMenu.content_panels + [
    InlinePanel('menu_items', label='Подменю в нашем меню'),
]


class MainMenuPage(Page):
    class Meta:
        verbose_name = u'Меню на главной странице'

    subpage_types = ['SectionMenu']

    @property
    def sections(self):
        sections = SectionMenu.objects.live().descendant_of(self)
        return sections


class CarMenuItemsPage(Orderable, MenuItemPage):
    page = ParentalKey('CarMenuPage', related_name='car_menu_items')


class CarMenuPage(Page, LinkFields):
    class Meta:
        verbose_name = u'Меню к автомобилю'

    @property
    def pages(self):
        pages = MenuItemPage.objects.live().descendant_of(self)
        return pages


CarMenuPage.content_panels = CarMenuPage.content_panels + [
    InlinePanel('car_menu_items', label='Разделы в меню автомобиля')
]
