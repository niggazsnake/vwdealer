$(function(){

	var tRotation = {

		curIndex : 0,
		collection : $('.tizer_wrapper'),
		length : $('.tizer_wrapper').length,
		showTime : 10000,
		animationTime : 500,

		timer : null,

		start : function() {

			if(tRotation.length > 1) {
				tRotation.timer = setInterval(function(){

					$(tRotation.collection[tRotation.curIndex]).animate({
						opacity:0
					}, tRotation.animationTime, function(){

						tRotation.curIndex++;
						if(tRotation.curIndex >= tRotation.length)
							tRotation.curIndex = 0;

						$(tRotation.collection[tRotation.curIndex]).animate({
							opacity:1
						}, tRotation.animationTime);

					})


				}, tRotation.showTime);
			}
		}
	}

	tRotation.start()
})