/**
 * Created by Yura on 01.04.15.
 */
$(document).ready(function(){
    // start ios
    $('.ios').iosSlider({
        desktopClickDrag: false,
        snapToChildren: true,
        infiniteSlider: true,
        autoSlide: true,
        autoSlideTimer: slideTime,
        autoSlideTransTimer: 1000
    });

	$('.slider_content').removeClass('hidden');
})