// callbacks
function sliderLoadedChange(args){
  args.sliderObject.parents('.ios').find('.goTo div').removeClass('selected');
  args.sliderObject.parents('.ios').find('.goTo div:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
}

$(document).ready(function() {

  // make navigation points
  $('.ios').each(function() {
    var slidesQ = $(this).find('.slider > *').length,
      i = 0;

    while (i < slidesQ) {
      $(this).find('.goTo').append('<div class="goTo_item"></div>');
      i++;
    }
  });

	// start ios
	$('.ios').iosSlider({
		desktopClickDrag: false,
		snapToChildren: true,
		infiniteSlider: true,
		autoSlide: true,
		autoSlideTimer: 5000,
		autoSlideTransTimer: 1000,
    onSliderLoaded: sliderLoadedChange,
    onSlideChange: sliderLoadedChange
	});

  // navigation points
  $('.ios .goTo div').on('click', function(){
    $(this).parents('.ios').iosSlider('goToSlide', $(this).index() + 1);
  });


	// arrows
	$('.slider_wrapper .arrow.prev').on('click', function(){$(this).parent('.ios').iosSlider('prevSlide');});
	$('.slider_wrapper .arrow.next').on('click', function(){$(this).parent('.ios').iosSlider('nextSlide');});

	$('.slider_wrapper').each(function(){
		var item_amont = $(this).find('.item').length;
		if (item_amont<2) {
			$(this).find('.arrow').hide();
		}
	});

	// autoplay
	$('.ios.no_autoplay').iosSlider('autoSlidePause');








	//ios для галереи
	$('.ios_gallery').iosSlider({
		snapToChildren: true,
		desktopClickDrag: true,
		infiniteSlider: false, 
		navSlideSelector: $('.ios_gallery_buttons .button'),
		onSlideChange: slideContentChange,
		onSliderLoaded: slideContentChange,
		onSlideComplete: slideComplete,
		navNextSelector: $('.ios_gallery .next'),
		navPrevSelector: $('.ios_gallery .prev'),
	});


	//disable arrows
	function slideComplete(args) {				
		$('.next, .prev').removeClass('inactive');
	    if(args.currentSlideNumber == 1) {
	        $('.prev').addClass('inactive');
	    } else if(args.currentSliderOffset == args.data.sliderMax) {
	        $('.next').addClass('inactive');
	    }
	}

	//определяем направление пролистывания
	function nextSliding () {
		$('.ios_gallery').addClass('next_sliding');
		$('.ios_gallery').removeClass('prev_sliding');
	}
	function prevSliding () {
		$('.ios_gallery').addClass('prev_sliding');
		$('.ios_gallery').removeClass('next_sliding');
	}
	$('.slider_wrapper .arrow.prev').on('click', function(){
		prevSliding ();
	});
	$('.slider_wrapper .arrow.next').on('click', function(){
		nextSliding ();
	});

	$('.ios_gallery_buttons .button').on('click', function(){
		var this_i = $(this).index();
		var current_i = $('.ios_gallery_buttons .button.current').index();
		if (this_i > current_i) {
			nextSliding ();
		}
		else {
			prevSliding ();
		}
	});

	//функция при срабатывании слайдера	
	function slideContentChange(args) {
		$('.ios_gallery_buttons .button').removeClass('current active');
		$('.ios_gallery_buttons .button:eq(' + (args.currentSlideNumber - 1) + ')').addClass('current active');

	    //если вперед крутим, то вызываем функцию для прокручивания слайдера превьюшек вперед
	    if ($('.ios_gallery').hasClass('next_sliding')) {
	    	previewGalleryNext ();
	    }
	    //если назад крутим, то вызываем функцию для прокручивания слайдера превьюшек назад
	    if ($('.ios_gallery').hasClass('prev_sliding')) {
	    	previewGalleryPrev ();
	    }
	}

	var slider_w = $('.ios_gallery_buttons').width(),  //ширина контейнера слайдера
		items_margin = 5; //отступ менжду превьюшками
        item_w_b = $('.ios_gallery_buttons .item').outerWidth(), //ширина большой превьюшки (активной)
        item_w = - $('.ios_gallery_buttons .item').next().outerWidth(); //ширина обычной превьюшки
        amount = $('.ios_gallery_buttons .item').length - 1; //считаем кол-во превьюшек за минусом большой

    //считаем ширину для слайдер и задаем ему эту ширину, чтобы можно было двигать влево-вправо
    var full_w = -item_w * amount + item_w_b;
    $('.ios_gallery_buttons .slider').width(full_w);

    //крутилочка превьюшек вперед
    function previewGalleryNext () {
        var offset = $('.ios_gallery_buttons .button.current').position();
        var slider_offset = $('.ios_gallery_buttons .slider').position();


        
        var cur_w = $('.ios_gallery_buttons .button.current').width();
    	var offset_rigth = offset.left + cur_w;
    	var offset_right_sl = -slider_offset.left+slider_w;
    	var offset_right_an = -slider_offset.left+slider_w+ cur_w + items_margin;

         //если позиция превьюшки больше позиции слайдера + ширина его контейнера, то слайдер двигаем влево
        if (offset_rigth == offset_right_an) {
            $('.ios_gallery_buttons .slider').animate({"left": "-=75px"},"linear");
        }
        else if (offset_rigth > offset_right_sl) {
        	var razn = -slider_offset.left+slider_w - offset_rigth;
    		$('.ios_gallery_buttons .slider').animate({"left": slider_offset.left+razn},"linear");
         }
    }

    //крутилочка превьюшек назад
    function previewGalleryPrev () {
        var offset = $('.ios_gallery_buttons .button.current').position();
        var slider_offset = $('.ios_gallery_buttons .slider').position();

        var cur_w = $('.ios_gallery_buttons .button.current').width();
    	var offset_rigth = offset.left + cur_w + items_margin;

        //если позиция превьюшки меньше позиции слайдер относительно контейнера, то слайдер двигаем вправо
        if (offset_rigth == -slider_offset.left) {
            $('.ios_gallery_buttons .slider').animate({"left": "+=75px"},"linear");
        }
        else if (offset.left < -slider_offset.left) {
        	var razn = -slider_offset.left - offset.left;
    		$('.ios_gallery_buttons .slider').animate({"left": slider_offset.left+razn},"linear");
         }
    }

    var max_left = full_w - slider_w - items_margin;
    $('.gallery_preview_wrapper .arrow').hover(function(){
    	if ($(this).hasClass("next")) {
    		$('.gallery_preview_wrapper .arrow.prev').removeClass('inactive');
    		$(this).parent().find('.slider').animate({"left": -max_left},2000,'linear',function(){
    			$('.gallery_preview_wrapper .arrow.next').addClass("inactive");
    		});
    	}
    	else if ($(this).hasClass("prev")) {
    		$('.gallery_preview_wrapper .arrow.next').removeClass('inactive');
    		$(this).parent().find('.slider').animate({"left": "0"},2000,'linear',function(){
    			$('.gallery_preview_wrapper .arrow.prev').addClass("inactive");
    		});
    	}
    },function(){
    	var this_slider = $(this).parent().find('.slider');
    	 
    	this_slider.stop();

    });



    var am = $('.ios_gallery_buttons').find('.button').length;
    if (am < 10) {
    	 $('.ios_gallery_buttons').siblings('.arrow').hide();
    }


});

