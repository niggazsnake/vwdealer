$(document).ready(function(){
	function getCars(){
		$('.model_list_wrapper .ml_item').width(0).fadeTo(0, 0);
		$.ajax({
			dataType: 'json',
			url: $('#filterForm').attr('action'),
			data: $('#filterForm').serialize(),
			success: function(data){


				$('.model_list_wrapper .vw_preloader').fadeTo(400, 0, function(){
					$(this).remove();
				});
				for (var i in data){
					$('#' + data[i].ID).animate({'width': 125, 'opacity': 1}, 500);
				}
			},
			error: function(){

			}
		})
	}

	function checkAll(){
		$('#filterForm, .value').each(function(){
			$(this).find('label').addClass('checked');
			$(this).find('input').addClass('chk');
			$(this).find('input').prop('checked', true);
		})
	}
	$('#filterForm input[type=checkbox]').on("click", getCars);
	$("#clear2").on("click", function(){
		checkAll();
		getCars();
	});
	checkAll();
});