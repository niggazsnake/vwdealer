$(document).ready(function(){


// одинаковая высота - запускаем до табов!!!
	function equalHeightSpecial(group) {
		var tallest = 0;
		group.each(function() {
			var thisHeight = $(this).height();
			if(thisHeight > tallest) {
				tallest = thisHeight;
			}
		});
		group.height(tallest);
	}
	
	equalHeightSpecial($(".js_auto_height .al_info "));
//~~~~~~~~~~~~~~~~~~~~~~~~~


// табы
$('.js_ui_tabs_wrapper').tabs({
    activate: function() {
        if($('.complect_detail_wrapper .ui-tabs-active').hasClass('select_hide')) {
            $('#complect_select').addClass('hidden');
        }
        else {
            $('#complect_select').removeClass('hidden');
        }

    }
});




// выбор количества в инпуте
$( ".spinner" ).spinner({ min: 0 });





// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


// диапазон цен
if($('#price-range').length){
	
	$( '#price-range' ).rangeSlider({
		arrows:false,
		symmetricPositionning: true,
		bounds: {min: 454944, max: 3044800},
		defaultValues:{min: 454944, max: 3044800},
		step: 1,
		formatter:function(val){
			var price = accounting.formatMoney(val, "", 0, " ", ",", "%v%s");
			return price;
		}

	});


		
		var basicValues = $('#price-range').rangeSlider('values');
	// ставим в инпуты начальные значения при загрузке
		$('#f_price_from').val(basicValues.min);
		$('#f_price_to').val(basicValues.max);
	
		$('.f_price_from').html(accounting.formatMoney(basicValues.min, "", 0, " ", ",", "%v%s"));
		$('.f_price_to').html(accounting.formatMoney(basicValues.max, "", 0, " ", ",", "%v%s"));	
		
		$('#price-range').on('valuesChanging', function(e, data){
	// меняем инпуты при изменении слайдера
			$('#f_price_from').val(data.values.min);
			$('#f_price_to').val(data.values.max);		
		

			$('.f_price_from').html(accounting.formatMoney(data.values.min, "", 0, " ", ",", "%v%s"));
			$('.f_price_to').html(accounting.formatMoney(data.values.max, "", 0, " ", ",", "%v%s"));
		});
	
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~









});