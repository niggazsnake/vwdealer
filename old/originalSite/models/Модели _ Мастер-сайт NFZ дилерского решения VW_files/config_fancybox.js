$(document).ready(function() {

// рандомный элемент закрывает фенсибокс
    $('.fancybox_close').on('click',function(){
        $.fancybox.close();
        return false;
    });



// обычный фенсибокс
    $('.fancybox').fancybox({
        padding: 0,
        wrapCSS: 'vw_ns',
        minHeight: 10,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.25)'
                }
            }
        }

    });

// фенсибокс для картинок
    $('.img_fancybox').fancybox({
        padding: [40,10,10,10],
        minHeight: 10,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.25)'
                }
            }
        }

    });
	
	
	
});