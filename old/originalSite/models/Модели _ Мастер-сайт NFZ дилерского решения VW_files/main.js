$(document).ready(function(){


	//$('body').append('<div style="position:fixed;left:5px;top:50px;z-index:100;"><a href="index.html">главная</a><br /><a href="offers.html">акции</a><br /><a href="offers_detail.html">акции детальная</a><br /><a href="offers_category.html">акции категории</a><br /><a href="news.html">новости</a><br /><a href="news_detail.html">новости детальная</a><br /><a href="404.html">404 страница</a><br /><a href="site_map.html">карта сайта</a><br /><a href="search.html">поиск</a><br /><a href="search_result.html">поиск результаты</a><br /><a href="form.html">форма</a><br /><a href="index_seo.html">ceo главная</a><br /><a href="model_seo.html">ceo модель</a><br /><br /><a href="models.html">модели</a><br /><a href="model_start.html">модель стартовая</a><br /><a href="model_gallery.html">модель галерея</a><br /><a href="model_tech.html">модель тех. характеристики</a><br /><a href="model_press.html">модель пресса</a><br /><a href="model_view.html">модель обзор</a><br /><a href="model_complects.html">модель комплектации</a><br /><a href="complect_detail.html">комплектация детальная</a><br /><a href="complect_compare.html">комплектация сравнение</a><br /><br /><a href="instock.html">АВН</a><br /><a href="instock_list.html">АВН список</a><br /><a href="instock_detail.html">АВН детальная</a><br /><br /><br /><a href="index_buttons.html">кнопки</a><br /><a href="index_grid_examples.html">сетка</a><br /></div>');


	$('.header_nav .top_lvl').hover(
		function(){
			var currentHeight = $(this).find('.header_nav_submenu').innerHeight();
			$(this).find('.submenu_wrapper').stop().animate({'height':currentHeight},300);
			$(this).addClass('hovered');
		},function(){
			$(this).find('.submenu_wrapper').stop().animate({'height':0},150,function(){$(this).parent().removeClass('hovered');});
		});


    $('.menu_inside .section_label').each(function(){
        $(this).css({'min-height':($(this).parents('.menu_inside').height() * 1.01)});
    });

// equal heights
	function equalHeight(group) {
		var tallest = 0;
		group.each(function() {
			var thisHeight = $(this).height();
			if(thisHeight > tallest) {
				tallest = thisHeight;
			}
		});
		group.height(tallest);
	}

	equalHeight($(".complect_list_item"));
	equalHeight($(".sof_art_item "));


//	equalHeight($(".group1"));
//	equalHeight($(".group2"));
//~~~~~~~~~~~~~~~~~~~~~~~~~



// crossbrowser placeholder in forms

	if(!Modernizr.input.placeholder) {

		$('input').each(function(){
			if($(this).attr('placeholder')){$(this).addClass('placeholder');}
		});

		$('input[type=password]').addClass('password');

		var phTarget = $('input.placeholder');

		phTarget.each(function(){
			if($(this).val() == '') {
				thisPlaceholder = $(this).attr('placeholder');
				$(this).val(thisPlaceholder);
			}
		});

		phTarget.focus(function(){
			if($(this).val() == $(this).attr('placeholder')) {
				$(this).val('');
			}
		});
		phTarget.blur(function(){
			if($(this).val() == '') {
				thisPlaceholder = $(this).attr('placeholder');
				$(this).val(thisPlaceholder);
			}
		});

		phTarget.parents('form').submit(function(){
			if($(this).val() == $(this).attr('placeholder')) {
//				return false;
			}
		});
	}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// CUSTOM CHECKS
	$('label.checkbox input:checked').parent().addClass('checked');

	$(document).on('change','label.checkbox input',function(){
		if($(this).is(':checked')){$(this).parent().addClass('checked');}
		else{$(this).parent().removeClass('checked');}
	});

// CUSTOM RADIO
	function customCheck(checkTarget){
		if(checkTarget.is(':checked')){
			checkTarget.parent().siblings().removeClass('checked');
			checkTarget.parent().addClass('checked');
		}
		else{
			checkTarget.parent().removeClass('checked');
		}
	}

// on change
	$(document).on('change','label.radio input',function(){
		customCheck($(this));
	});
// ORDERS LIST
	$('.vw_order_info').each(function() {
		var contentHeight = $(this).find('.vw_order_info_content').innerHeight();
		$(this).attr('data-height',contentHeight);
		if(!$(this).hasClass('opened_info')) {
			$(this).find('.vw_order_info_wrapper').height(0);
		}
	});

	$('.vw_order_info_trigger').on('click',function(){
		if($(this).parents('.opened_trigger').length){
			$(this).text($(this).attr('data-close')).closest('tr').removeClass('opened_trigger').next('.vw_order_info').removeClass('opened_info').find('.vw_order_info_wrapper').stop().animate({'height':0},300);
		}
		else{
			targetHeight = $(this).closest('tr').addClass('opened_trigger').next('.vw_order_info').attr('data-height');
			$(this).text($(this).attr('data-open')).closest('tr').addClass('opened_trigger').next('.vw_order_info').addClass('opened_info').find('.vw_order_info_wrapper').stop().animate({'height':targetHeight},300);
		}

		return false;
	});
// ~~~~~~~~~~~~~~~~~~~~~~~~


// MODEL LIST ANIMATION

	/*$('.model_list_wrapper .ml_item').width(0).fadeTo(0,0);

	$(window).load(function(){
		$('.model_list_wrapper .vw_preloader').fadeTo(400,0, function(){$(this).remove();});

		$('.model_list_wrapper .ml_item').each(function(){
			$(this).animate({'width':125,'opacity':1},500);
		});
	});*/





// VW SLIDEDOWN
	$(document).on('click','.vw_slidedown_trigger',function(){
		if(!$(this).parents('.vw_slidedown').hasClass('opened')){
			$(this).parents('.vw_slidedown').find('.vw_slidedown_content').stop().slideDown(500,function(){$(this).parents('.vw_slidedown').addClass('opened');});
		}
		else {
			$(this).parents('.vw_slidedown').find('.vw_slidedown_content').stop().slideUp(300,function(){$(this).parents('.vw_slidedown').removeClass('opened');});
		}
	});


// VW SHOWHIDE

	$(document).on('click','.vw_showhide_trigger',function(){
		if(!$(this).parents('.vw_showhide').hasClass('shown')){

			$('.vw_showhide.shown').removeClass('shown').find('.vw_showhide_content').hide(0);
			$(this).parents('.vw_showhide').find('.vw_showhide_content').show(0,function(){$(this).parents('.vw_showhide').addClass('shown');});

		}
		else {
			$(this).parents('.vw_showhide').find('.vw_showhide_content').hide(0,function(){$(this).parents('.vw_showhide').removeClass('shown');});
		}
	});



// SHOW BUBBLES
// data-bubble-size: 'default', 'longer', 'longest'
	$(document).on('mouseover','.vw_hasbubble',function(){
		if($(this).find('.vw_bubble_wrapper').length){
			$(this).find('.vw_bubble_wrapper').show();
		}
		else {
			$(this).append('<div class="vw_bubble_wrapper '+$(this).attr('data-bubble-size')+'"><div class="vw_bubble"><div class="b_inner">'+$(this).attr('data-bubble-text')+'</div></div></div>');
		}

	});






//teaser animation
	var teaser_normal =  158,
		teaser_big =  206,
		teaser_small = 126;
	$('.js_teaser').mouseover(
		function(){
			$(this).parent().find(".img_wrap").stop();
			$(this).find(".img_wrap").animate({"width": teaser_big}, 200, "easeOutQuad");
			$(this).next().find(".img_wrap").animate({"width": teaser_normal}, 200,  "easeOutQuad");
			$(this).next().nextAll().find(".img_wrap").animate({"width": teaser_small}, 200,  "easeOutQuad");
			$(this).prev().find(".img_wrap").animate({"width": teaser_normal}, 200,  "easeOutQuad");
			$(this).prev().prevAll().find(".img_wrap").animate({"width": teaser_small}, 200,  "easeOutQuad");
		}
	);
	$('.js_teaser_wrapper').mouseleave(function() {
		$(this).find(".img_wrap").stop().animate({"width": teaser_normal}, 200,  "easeOutQuad");
	});


//show model menu
	var top = $('.model_menu_content').outerHeight() + 2;
	$('.model_menu_content').css("margin-top", -top);
	$('.js_models_menu').click(function(){
		if ($(this).hasClass('opened')) {
			$(this).parent().siblings('.model_menu_content').animate({"margin-top": -top}, 500, 'easeOutQuad');
		}
		else {
			$(this).parent().siblings('.model_menu_content').animate({"margin-top": "0"}, 500, 'easeOutQuad');
		}
		$(this).toggleClass('opened');


	});


//show dowloads pop in gallery
	$('.js_hover_pop').hover(function(){
		$(this).find('.js_pop').fadeIn(400);
	},function(){
		$(this).find('.js_pop').fadeOut(400);
	});


	$('.gallery_preview_slider .item').hover(function(){
		if (!$(this).hasClass('current')) {
			$(this).addClass('active');
			$(this).siblings('.current').removeClass('active');
		}

	},function(){
		if (!$(this).hasClass('current')) {
			$(this).removeClass('active');
			$(this).siblings('.current').addClass('active');
		}
	});





//		$('.nfl_city_list').hide();

// ВЫБИРАЛКА ГОРОДОВ

// склоняем города
	function getNumEnding(iNumber, aEndings){
		var sEnding, i;
		iNumber = iNumber % 100;
		if (iNumber>=11 && iNumber<=19) {
			sEnding=aEndings[2];
		}
		else {
			i = iNumber % 10;
			switch (i)
			{
				case (1): sEnding = aEndings[0]; break;
				case (2):
				case (3):
				case (4): sEnding = aEndings[1]; break;
				default: sEnding = aEndings[2];
			}
		}
		return sEnding;
	}

// city count
	function nflCityChoose(currentSelector){
		$(currentSelector).each(function(){
			var nflCityCount = $(this).find('.nfl_city_list input:checked').length;
			if(nflCityCount > 1){
				$(this).find('select option').text('Выбрано '+nflCityCount+' элемент'+getNumEnding(nflCityCount,['', 'а', 'ов']));
			}
			else if(nflCityCount < 1){
				$(this).find('select option').text('Выберите значение');
			}
			else {
				var choosenCity = $(this).find('.nfl_city_list input:checked').parent().text();
				$(this).find('select option').text(choosenCity);
			}
		});
	}

// choose city
	if($('.nfl_city_list_wrapper').length){



//	in case of F5
		nflCityChoose('.nfl_city_list_wrapper');

		$('.select_click_trigger').click(function(){
			var cityParent = $(this).parents('.nfl_city_list_wrapper');

			if(cityParent.hasClass('city_opened')){
				//$('.city_opened .nfl_city_list').hide();
				$('.city_opened').removeClass('city_opened');
			}
			else {
//			$('.city_opened .nfl_city_list').hide();
				$('.city_opened').removeClass('city_opened');
				cityParent.addClass('city_opened');
//			cityParent.find('.nfl_city_list').show();
				//	$('.nfl_city_list').mCustomScrollbar("update");


			}
		});

//  not so good imitation of hover
		$('.select_click_trigger').hover(function(){
			$(this).parents('.nfl_city_list_wrapper').find('select').focus();
		},function(){
			$(this).parents('.nfl_city_list_wrapper').find('select').blur();
		});


		$('.nfl_city_list label').click(function(){
			nflCityChoose('.city_opened');
		});


// - stop propagation
		$(document).click(function(event) {
			if ($(event.target).closest('.nfl_city_list_wrapper.city_opened').length ) return;
			// hide sub
			//	$('.city_opened .nfl_city_list').hide();
			$('.city_opened').removeClass('city_opened');
			event.stopPropagation();
		});
	}

//aside menu accordeon
	$('.js_accordeon').click(function(){
		$(this).parent().addClass('active');
		$(this).siblings(".ul_sub").slideDown();
		$(this).parent().siblings().removeClass('active');
		$(this).parent().siblings().find(".ul_sub").slideUp();
	});


	$('.js_tab').click(function(e){
		$(this).addClass('active').siblings().removeClass('active');
		var data =$(this).attr('data-tab');
		$(this).parent().siblings(".js_tab_content").removeClass('active');
		$(this).parent().siblings(".js_tab_content."+data).addClass('active');
		$(".ios").iosSlider('update');
		e.preventDefault();
	});



//show complects popup in compare
	$(document).on('click','.js_complect_pop', function(e){
		$(this).siblings(".complect_pop").show();
		$(this).parents('.complect_list_item').siblings().find('.complect_pop').hide();
		$('body').addClass('js_close');
		e.preventDefault();
	});
	$(document).on('click','.js_close', function(e){
		$('.complect_pop').hide();
		$('body').removeClass('js_close');
		e.preventDefault();
	});


//content accordeon
	$('.js_accor').click(function(e){
		$(this).next(".accor_content").slideToggle();
		e.preventDefault();
	});









//hide or show differences in compare
//функция показать только различия
	function difference_hide() {

		var Tables = $(".js_compare_diffs table");

		for(i = 0; i < Tables.length; i++) {
			var Rows = $(Tables[i]).find("tr");

			for(j = 0; j < Rows.length; j++) {
				var Cells = $(Rows[j]).children("td.tbb_gray");
				var value = null;
				var HideRow = true;

				for(p = 0; p < Cells.length; p++) {
					if(value == null)
						value = $(Cells[p]).children("div").attr("class");
					if($.trim($(Cells[p]).html()) != "") {
						if(value != $(Cells[p]).children("div").attr("class")) {
							HideRow = false;
							break;
						}
					}
				}

				if(HideRow == true) {
					$(Rows[j]).addClass("hidden");
				}
			}
		}

		// спрятали строки, но могли остаться заголовки без таблиц - ай-яй-яй
		for(i = 0; i < Tables.length; i++) {
			if($(Tables[i]).find("tr.hidden").length == $(Tables[i]).find("tr").length) {
				$(Tables[i]).parents('.compare_table_wrapper').addClass("hidden");
			}
		}


	}

//показать не только различия
	function difference_show() {
		$(".js_compare_diffs tr.hidden, .js_compare_diffs .compare_table_wrapper.hidden").removeClass("hidden");
	}



//если чекбокс чекнут, то показывает только различия
	if ($('#show_diff input').is(':checked')) {
		difference_hide();
	}

//когда чекаем чекбокс, то вызывает функцию "показать различия" или наоборот
	$(document).on('change','#show_diff input',function(){
		if($(this).is(':checked')){
			difference_hide();
		}
		else{
			difference_show();
		}
		$(".table_blue_borders").find("tr").removeClass('first last');
		$(".table_blue_borders").find("tr:visible:first").addClass('first');
		$(".table_blue_borders").find("tr:visible:last").addClass('last');
	});





//ajax на детальной комплектации
	$(document).on('change','.js_ajax',function(){


		var form_data = $(this).parents('form').serialize();
		var url = $(this).parents('form').attr('action');
		//console.log(form_data);

		$.ajax({
			type: "post",
			data: form_data,
			url: url,
			//url: "ajax_standart.php",
			//url: "ajax_optional.php",
			dataType: 'json',
			success: function(data){

				var st_flag = false;
				for(i in data.STANDART) {
					st_flag = true;
				}

				var opt_flag = false;
				for(i in data.OPTIONAL) {
					opt_flag = true;
				}

				if (st_flag) {

					$('#STANDART_table').empty();
					$('#tab_dafault_options').removeClass("hidden");
					$('a[href="#tab_dafault_options"]').removeClass("hidden");

					for(i in data.STANDART) {
						var tr = $('<tr></tr>');
						var fstTd = $('<th>'+i+'</th>');
						var scdTd = $('<td class="tbb_the_gap"></td>');
						var thdTd = $('<td class="tbb_gray"></td>');
						var ul = $('<ul class="nomargin"></ul>');
						for(j in data.STANDART[i]) {
							var li = $('<li>'+data.STANDART[i][j]+'</li>');
							li.appendTo(ul);
						}
						ul.appendTo(thdTd);
						fstTd.appendTo(tr);
						scdTd.appendTo(tr);
						thdTd.appendTo(tr);

						tr.appendTo('#STANDART_table');

					}
				}
				else {
					$('#tab_dafault_options').addClass("hidden");
					$('a[href="#tab_dafault_options"]').addClass("hidden");
					$('.js_ui_tabs_wrapper').tabs( "option", "active", 2 );
				}

				if (opt_flag) {

					$('#OPTIONAL_table').empty();
					$('#tab_other_options').removeClass("hidden");
					$('a[href="#tab_other_options"]').removeClass("hidden");

					for(i in data.OPTIONAL) {
						var tr = $('<tr></tr>');
						var fstTd = $('<th>'+i+'</th>');
						var scdTd = $('<td class="tbb_the_gap"></td>');
						var thdTd = $('<td class="tbb_gray"></td>');
						var ul = $('<ul class="nomargin"></ul>');
						for(j in data.OPTIONAL[i]) {
							var li = $('<li>'+data.OPTIONAL[i][j]+'</li>');
							li.appendTo(ul);
						}
						ul.appendTo(thdTd);
						fstTd.appendTo(tr);
						scdTd.appendTo(tr);
						thdTd.appendTo(tr);

						tr.appendTo('#OPTIONAL_table');
					}
				}
				else {
					$('#tab_other_options').addClass("hidden");
					$('a[href="#tab_other_options"]').addClass("hidden");
					$('.js_ui_tabs_wrapper').tabs( "option", "active", 1 );
				}

			}
		});


	});


	//сравнение комплектаций


	//считаем кол-во комплектаций в сравнении, чтобы удалить/добавить крестики и "показать различия"
	function complects_amount () {
		var comp_amount = $(".complect_list_item.filled").length;

		if (comp_amount<2) {
			$('.js_compare_delete').addClass('hidden');
			$('#show_diff').addClass('hidden');
			difference_show();
		}
		else {
			$('.js_compare_delete').removeClass('hidden');
			$('#show_diff').removeClass('hidden');
			if ($('#show_diff').find('input').is(':checked')) {
				difference_hide();
			}
		}

		$(".complect_list_item").each(function(){
			var index = $(this).index();

			$(this).find(".complect_pop").removeClass("right center");
			if (index==2) {
				$(this).find(".complect_pop").addClass("center");
			}
			else if (index==3) {
				$(this).find(".complect_pop").addClass("right");
			}
		});

	}

	//поп-ап с комплектациями
	//var complect_pop_link_default = $('#complect_pop_link_default').html();
	var complect_pop_link_default = $('.complect_pop_link:first').html();
	var complect_default = ('<div class="grid_1 push_1 complect_list_item default"><div class="header_item no_background"><div class="img"><img src="/local/templates/vw45d/img/file.jpeg"></div></div><div class="btns_wrapper">'+complect_pop_link_default+'</div></div>')


	//удаление комплектации и всех сопутствующих, если жмякаем на крестик + добавление пустого окошка для комплектации
	$(document).on('click','.js_compare_delete',function(){

		var that_index = $(this).parents(".complect_list_item").index();

		$(this).parents('.complect_list_item').remove();
		$('.compare_list').append(complect_default);

		$('.complect_list_table tr').each(function(){
			$(this).find('td.tbb_gray').eq(that_index-1).remove();
			$(this).find('td.tbb_gray').eq(that_index-1).prev('td').remove();
		});

		$('.js_ajax_engine').find('.grid_1').eq(that_index-1).remove();
		$('.js_ajax_comp_links').find('.grid_1').eq(that_index-1).remove();

		complects_amount ();

	});



	//заготовки элементов
	var delete_html = ('<a class="vw_btn btn_func btn_hasicon btn_notext btn_close js_compare_delete"></a>');
	var link_html = ('<a href="#" class="vw_btn hasarrow margintop05 js_ajax_link">Подробнее</a>');
	var engines_html = ('<div class="grid_1 push_1"><div class="title">Двигатель</div><select name="ENGINES[]" class="js_ajax_engine_select"></select></div>');
	var comp_link_html = ('<div class="grid_1 push_1 txt_right"><a href="#" class="vw_btn hasarrow js_ajax_link">Подробнее</a></div>');


	function ajax_compare () {

	}

	//выбрать комплектацию и добавить через ajax
	$(document).on('click','.js_choose_compare',function(e) {

		var that_parent = $(this).parents(".complect_list_item");
		var that_index = that_parent.index();

		var comp_id = $(this).attr('id');

		$(this).addClass('active').siblings().removeClass('active');
		$(this).parents('.complect_pop').hide();

		var that_select = $('.js_ajax_engine').find('.grid_1').eq(that_index-1).find('select');
		that_select.empty();
		$('<option value=""></option>').appendTo(that_select);

		if(that_parent.find('.header_item').find('input[name="COMPS[]"]').length > 0)
			that_parent.find('.header_item').find('input[name="COMPS[]"]').val(comp_id);
		else
			that_parent.find('.header_item').append('<input type="hidden" name="COMPS[]" value="'+comp_id+'" />');



		var form_data = $(this).parents('form').serialize();
		var url = $(this).parents('form').attr('action');

		$.ajax({
			type: "post",
			url: url,
			data: form_data,
			dataType: 'json',
			success: function(data){

				if (that_parent.prev().hasClass('default')) {
					that_parent.prev().remove();
					$('.compare_list').append(complect_default);
				}

				var that_index = that_parent.index();

				if (that_parent.hasClass('default')) {
					that_parent.prepend(delete_html);
					that_parent.addClass('filled').removeClass('default');
					that_parent.find('.btns_wrapper').append(link_html);
					that_parent.find('.header_item').append('<div class="name"></div>')
					that_parent.find('.header_item').append('<div class="price">От <span class="price_item"></span> руб.</div>')

					$('.js_ajax_engine').append(engines_html);
					$('.js_ajax_comp_links').append(comp_link_html);
				}

				that_parent.find('input[name="COMPS[]"]').val(comp_id);

				var def_engine = data.COMPS[comp_id]['DEFAULT_ENGINE'];
				//that_parent.find('input[name=""]').val(def_engine);

				var name = data.COMPS[comp_id]['NAME'];
				that_parent.find('.name').html(name);


				var img = data.COMPS[comp_id]['DETAIL_PICTURE'];
				that_parent.find('.img').html('<img src="'+img+'">');

				that_parent.find('.price_item').html(data.COMPS[comp_id].FORMATED_PRICE);

				var link = data.COMPS[comp_id]['DETAIL_PAGE_URL'];
				that_parent.find('.js_ajax_link').attr('href',link);

				// на случай, если раньше не было селекта - заново переопределим его
				that_select = $('.js_ajax_engine').find('.grid_1').eq(that_index-1).find('select');
				that_select.empty();
				$('.js_ajax_comp_links').find('.grid_1').eq(that_index-1).find('.js_ajax_link').attr('href',link);

				//$('.js_ajax_engine').find('.grid_1').eq(that_index-1).find('input[name="COMPS[]"]').val(comp_id);

				for(i in data.COMPS[comp_id]['ENGINES']) {
					var engine_item =  data.COMPS[comp_id]['ENGINES'][i];
					that_select.append("<option value='" + engine_item + "'>" + engine_item + "</option>");
				}




				//рисуем таблички
				$('.tab_item').empty();

				for(i in data.MATRIX) {
					//console.log(i);

					var data_container = i;
					//console.log(data_container);

					var parent = $('.tab_item[data-container="'+data_container+'"]');


					for(j in data.MATRIX[i]) {

						//console.log("==="+j);

						var compare_table_wrapper = $('<div class="compare_table_wrapper"></div>');
						compare_table_wrapper.appendTo(parent);

						var table_title = $('<div class="table_title">'+j+'</div>');
						table_title.appendTo(compare_table_wrapper);

						var table = $('<table class="table_blue_borders tbb_thirds complect_list_table"></table>');
						table.appendTo(compare_table_wrapper);


						for (k in data.MATRIX[i][j]) {
							//console.log("k----"+k);

							var tr = $('<tr></tr>');
							var fstTd = $('<th>'+k+'</th>');
							var margTd = ('<td class="tbb_the_gap"></td>');
							var contTd = ('<td class="tbb_gray"></td>');

							var st = ('<div class="st_icon"></div>');
							var opt = ('<div class="opt_icon"></div>');
							var not = ('<div class="not_icon"></div>');


							tr.appendTo(table);
							fstTd.appendTo(tr);

							var names = $('input[name="COMPS[]"]');
							var engines = $('select[name="ENGINES[]"]');

							var count = 0;
							$(".complect_list_item").each(function(){


								//var c_name = $(this).find('input[name="COMPS[]"]').val();
								//var c_engine = $(this).find('input[name="engine"]').val();

								var c_name = $(names[count]).val();
								var c_engine = $(engines[count]).val();

								var sr = (""+c_name+"/"+c_engine+"");
								//console.log(sr);
								for (d in data.MATRIX[i][j][k]) {

									var d_data = d;
									//console.log("data     "+d_data);


									if (sr == d_data ) {

										//console.log("sr +++++ "+sr);

										if (data.MATRIX[i][j][k][d] == 'st') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">'+st+'</td>');
										}
										else if (data.MATRIX[i][j][k][d] == 'op') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">'+opt+'</td>');
										}
										else if (data.MATRIX[i][j][k][d] == 'no') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">'+not+'</td>');
										}

									}
								}

								count++;

							});





						}

					}


				}

				complects_amount ();

				tabs_check_empty ();


			}
		});

		e.preventDefault();

	});

	function tabs_check_empty () {
		$('.tab_item').each(function(){
			var table_len = $(this).find('.compare_table_wrapper').length;

			if (table_len<1) {
				$(this).hide();
				var this_id = $(this).attr('id');
				//console.log(this_id);
				$('a[href="#'+this_id+'"]').parent().hide();
				if($('a[href="#'+this_id+'"]').parent().hasClass("ui-state-active")) {
					$('.js_ui_tabs_wrapper').tabs( "option", "active", 0 );
				}

			}
			else {
				$('a[href="#'+this_id+'"]').parent().show();
			}

		});
	}



	//аякс когда выбираем двигает в селекте
	$(document).on('change','.js_ajax_engine_select',function(){

		var c_engine = $(this).val();

		var form_data = $(this).parents('form').serialize();
		var url = $(this).parents('form').attr('action');
		//console.log(form_data);

		var select_val = $(this).val();

		var sel_index = $(this).parents(".grid_1").index();
		//console.log(sel_index);

		$('.compare_list').find('.complect_list_item').eq(sel_index).find('input[name="engine"]').val(select_val);

		$.ajax({
			type: "post",
			url: url,
			data: form_data,
			dataType: 'json',
			success: function(data){

				//рисуем таблички
				$('.tab_item').empty();

				for(i in data.MATRIX) {

					var data_container = i;

					var parent = $('.tab_item[data-container="'+data_container+'"]');

					for(j in data.MATRIX[i]) {

						var compare_table_wrapper = $('<div class="compare_table_wrapper"></div>');
						compare_table_wrapper.appendTo(parent);

						var table_title = $('<div class="table_title">'+j+'</div>');
						table_title.appendTo(compare_table_wrapper);

						var table = $('<table class="table_blue_borders tbb_thirds complect_list_table"></table>');
						table.appendTo(compare_table_wrapper);

						for (k in data.MATRIX[i][j]) {

							var tr = $('<tr></tr>');
							var fstTd = $('<th>'+k+'</th>');
							var margTd = ('<td class="tbb_the_gap"></td>');
							var contTd = ('<td class="tbb_gray"></td>');

							var st = ('<div class="st_icon"></div>');
							var opt = ('<div class="op_icon"></div>');
							var not = ('<div class="no_icon"></div>');

							tr.appendTo(table);
							fstTd.appendTo(tr);

							var names = $('input[name="COMPS[]"]');
							var engines = $('select[name="ENGINES[]"]');

							var count = 0;

							$(".complect_list_item").each(function(){

								//var c_name = $(this).find('input[name="COMPS[]"]').val();
								//var c_engine = $(this).find('input[name="ENGINE"]').val();

								var c_name = $(names[count]).val();
								var c_engine = $(engines[count]).val();

								var sr = (""+c_name+"/"+c_engine+"");

								for (d in data.MATRIX[i][j][k]) {

									var d_data = d;

									if (sr == d_data) {

										if (data.MATRIX[i][j][k][d] == 'st') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">' + st + '</td>');
										}
										else if (data.MATRIX[i][j][k][d] == 'op') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">' + opt + '</td>');
										}
										else if (data.MATRIX[i][j][k][d] == 'no') {
											tr.append(margTd);
											tr.append('<td class="tbb_gray">' + not + '</td>');
										}

									}
								}
								count++;

							});

						}

					}

				}

				tabs_check_empty ();

			}
		});



	});





});
