# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-16 10:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sections', '0006_auto_20160516_1045'),
    ]

    operations = [
        migrations.CreateModel(
            name='SectionPreviewLinks',
            fields=[
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='preview_items', to='sections.SectionPage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=('sections.sectionpreviewpage', models.Model),
        ),
    ]
