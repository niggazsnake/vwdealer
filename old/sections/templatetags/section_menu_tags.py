# -*- coding: utf-8 -*-
from django import template

from sections.models import MainSectionPage

register = template.Library()

def has_menu_children(page):
    return page.get_children().live().in_menu().exists()

@register.inclusion_tag('sections/menu.html', takes_context=True)
def section_menu(context, parent, calling_page=None):
    self = context['self']
    if isinstance(self, MainSectionPage):
        parent = self
    else:
        parent = self.get_ancestors().type(MainSectionPage).last()

    menu_items = parent.get_children().live().in_menu()
    for menuitem in menu_items:
        #menuitem.show_dropdown = has_menu_children(menuitem)
        # We don't directly check if calling_page is None since the template
        # engine can pass an empty string to calling_page
        # if the variable passed as calling_page does not exist.
        menuitem.active = (calling_page.url.startswith(menuitem.url)
                           if calling_page else False)
    return {
        'calling_page': calling_page,
        'menuitems': menu_items,
        'request': context['request'],
    }