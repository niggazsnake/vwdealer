# -*- coding: utf-8 -*-
from wagtail.wagtailcore import hooks
from django.utils.html import format_html

@hooks.register('insert_editor_js')
def editor_js():
    return format_html(
        """
        <script>
            registerHalloPlugin('hallohtml');
            registerHalloPlugin('hallojustify');
        </script>
        """
    )
