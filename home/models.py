# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.fields import RichTextField

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailadmin.edit_handlers import (FieldPanel,
                                                InlinePanel,
                                                MultiFieldPanel,
                                                PageChooserPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from teazers.models import TeazerIndexPage

class HomePage(Page):
    subpage_types = ['sections.SectionIndexPage', 'teazers.TeazerIndexPage']

    @property
    def teazer_index(self):
        teazer = TeazerIndexPage.objects.all()
        if teazer:
            return teazer[0]
        return None


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
    ]

    class Meta:
        abstract = True


class SlidePage(LinkFields):
    class Meta:
        verbose_name = u'Слайд на главной странице сайта'
        abstract = True

    title_page = RichTextField(u"Заголовок слайда", max_length=250)
    image_page = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        related_name='+'
    )

    panels = [
        FieldPanel('title_page'),
        ImageChooserPanel('image_page'),
        MultiFieldPanel(LinkFields.panels, "Ссылка на источник"),
    ]


class HomePageSlidePageItems(Orderable, SlidePage):
    page = ParentalKey('home.HomePage', related_name='slides')


HomePage.content_panels = [
    FieldPanel('title', classname="full title"),
    InlinePanel('slides', label='Слайды')
]