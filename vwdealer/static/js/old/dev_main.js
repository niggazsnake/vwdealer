/**
 * Created by Yuri on 28.04.2015.
 */
/*Удаляем картинку, если в ней есть ошибка*/
window.CheckImagesLoad = function(obj) {
    $(obj + '[src=""]').hide();
    $(obj).error(function() {
        $(this).hide()
    });
};

$(document).ready(function(){
    CheckImagesLoad('img');

    $("body").on("kdx-load", "form", function(){
        $(this).parent().kdxForm({
            strError: 'not_ok',
            strSubmitClass: '',
            objSubmitOpacity: {
                'ACTIVE':  'Y',
                'OPACITY': .5
            },
            OnBeforeFormSend : function(strFormCode){
                var objEvent = {
                    yandex: 'CUSTOMFORMSUBMIT',
                    google: [ strFormCode + 'Form', 'submit', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSubmit'],
                            google: [
                                objItem['objGAParams']['objSubmit']['strCategory'],
                                objItem['objGAParams']['objSubmit']['strEvent'],
                                objItem['objGAParams']['objSubmit']['strLabel']
                            ]
                        };

                        return false;
                    }
                });

                YaSend( objEvent['yandex'] );
                GaSend( objEvent['google'][0], objEvent['google'][1], objEvent['google'][2] );
            },
            OnFormSuccess : function(strFormCode){
                var objEvent = {
                    yandex: 'CUSTOMFORMSUCCESS',
                    google: [ strFormCode + 'Form', 'success', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSuccess'],
                            google: [
                                objItem['objGAParams']['objSuccess']['strCategory'],
                                objItem['objGAParams']['objSuccess']['strEvent'],
                                objItem['objGAParams']['objSuccess']['strLabel']
                            ]
                        };

                        return false;
                    }
                });

                YaSend( objEvent['yandex'] );
                GaSend( objEvent['google'][0], objEvent['google'][1], objEvent['google'][2] );
            }
        });
    });

    $("body").on("avn25-kdx-load", "form", function(){
        $(this).parent().kdxForm({
            strError: 'error',
            strSubmitClass: '',
            objSubmitOpacity: {
                'ACTIVE':  'Y',
                'OPACITY': .5
            },
            OnBeforeFormSend : function(strFormCode){
                var objEvent = {
                    yandex: 'CUSTOMFORMSUBMIT',
                    google: [ strFormCode + 'Form', 'submit', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSubmit'],
                            google: [
                                objItem['objGAParams']['objSubmit']['strCategory'],
                                objItem['objGAParams']['objSubmit']['strEvent'],
                                objItem['objGAParams']['objSubmit']['strLabel']
                            ]
                        };

                        return false;
                    }
                });

                YaSend( objEvent['yandex'] );
                GaSend( objEvent['google'][0], objEvent['google'][1], objEvent['google'][2] );
            },
            OnFormSuccess : function(strFormCode){
                var objEvent = {
                    yandex: 'CUSTOMFORMSUCCESS',
                    google: [ strFormCode + 'Form', 'success', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSuccess'],
                            google: [
                                objItem['objGAParams']['objSuccess']['strCategory'],
                                objItem['objGAParams']['objSuccess']['strEvent'],
                                objItem['objGAParams']['objSuccess']['strLabel']
                            ]
                        };

                        return false;
                    }
                });

                YaSend( objEvent['yandex'] );
                GaSend( objEvent['google'][0], objEvent['google'][1], objEvent['google'][2] );
            }
        });
    });

    $('.fancybox-ajax').fancybox({
        type: 'ajax',
        wrapCSS: 'vw_ns',
        padding: 0
    });

    if($('[name^="form_date"]').length > 0){
        $('[name^="form_date"]').on('click', function(){
            $(this).next().click();
        });
    }
});